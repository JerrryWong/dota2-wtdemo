function myrandom()
    local strTime = string.gsub( string.gsub( GetSystemTime(),':',''),'0','' )
    math.randomseed(tonumber(strTime))
end

function getIntPart(x)
    if x <= 0 then
        return math.ceil(x)
    end
    if math.ceil(x) == x then
        x = math.ceil(x)
    else
        x = math.ceil(x) - 1
    end
    return x
end

function getWeekDay()
    return tonumber(os.date("%w"))
end

-- 字符串是否包含中文
function IsHaveChinese(str)
    local strLen = #str
    for i = 1, strLen do
        local curByte = string.byte(str, i)
        if curByte > 127 then
            return true
        end
    end
    return false
end

--[[LuaLog]]
local function debugInfo( stackLv )
    local dinfo = debug.getinfo(stackLv)  --//Stack Level
    local source = dinfo.source
    local funcline = dinfo.linedefined
    local filename = dinfo.short_src
    local funcname = dinfo.name or "main"
    return source, funcline, filename, funcname
end

local function logPrint(stacklv)
    local source,funcline,filename,funcname = debugInfo(stacklv + 1)
    local str = "_______LuaLog:  __File = "..filename
                .."  __Function = "..funcname
                .."  __LineNo = "..funcline
                .."  __LogMsg = "
    return str
end

function Log(strLogInfo,stackLv)
    stackLv = stackLv or 3
    local msgPrint = logPrint(stackLv)
    msgPrint = msgPrint..strLogInfo
    print(msgPrint)
end