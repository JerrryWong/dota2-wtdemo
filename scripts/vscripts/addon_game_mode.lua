-- Generated from template
require("function")
require("init")

if CAddonWormWarGameMode == nil then
	CAddonWormWarGameMode = class({})
end

local function PrecacheEveryThingFromTable(context,kvtbl)
	for key ,value in pairs(kvtbl) do
		if type(value) == "table" then
			PrecacheEveryThingFromTable( context, value )
		else
			if string.find(value,"vpcf") then
				PrecacheResource("particle",value, context)
				Log(" Precache Particle value = "..value )
			end

			if string.find(value,"vmdl") then
				PrecacheResource("model",value, context)
				Log(" Precache Models value = "..value )
			end
			
			if string.find(value,"vsndevts") then
				PrecacheResource("soundfile",value, context)
				Log(" Precache SoundFiles value = "..value )
			end
		end
	end
end

local function PrecacheLocalThings(context)
	local kv_files = {
		"scripts/npc/npc_units_custom.txt",
		"scripts/npc/npc_abilities_custom.txt",
		"scripts/npc/npc_heroes_custom.txt",
		"scripts/npc/npc_abilities_override.txt",
		"npc_items_custom.txt" }

	for _, kv in pairs( kv_files ) do
		local kvs = LoadKeyValues(kv)
		Log( "Begin To Load Resource from table = "..kv )
		PrecacheEveryThingFromTable(context, kvs)
	end
end


function Precache( context )
	--[[
		Precache things we know we'll use.  Possible file types include (but not limited to):
			PrecacheResource( "model", "*.vmdl", context )
			PrecacheResource( "soundfile", "*.vsndevts", context )
			PrecacheResource( "particle", "*.vpcf", context )
			PrecacheResource( "particle_folder", "particles/folder", context )
	]]
	local dt = GameRules:GetGameTime()
	PrecacheLocalThings(context)
	PrecacheResource("particle_folder","particles/buildinghelper" ,context)
	PrecacheUnitByNameSync("npc_dota_hero_tinker",context)
	dt = dt - GameRules:GetGameTime()
	Log( "Loading cache Time = "..tostring(time).."Secs")
end

-- Create the game mode when we activate
function Activate()
	GameRules.AddonTemplate = CAddonWormWarGameMode()
	GameRules.AddonTemplate:InitGameMode()
end


function CAddonWormWarGameMode:InitGameMode()
	Log( "Template addon is loaded." )
	GameRules:GetGameModeEntity():SetThink( "OnThink", self, "GlobalThink", 2 )
	
	if not bMute then
		bMute = true
		self:InitUsers()
	end
	self:AddSelfEvents()
end

local bMute = false
function CAddonWormWarGameMode:InitUsers()
	initplayerstats()
end

-- Evaluate the state of the game
function CAddonWormWarGameMode:OnThink()
	if GameRules:State_Get() == DOTA_GAMERULES_STATE_GAME_IN_PROGRESS then
		--print( "Template addon script is running." )
	elseif GameRules:State_Get() >= DOTA_GAMERULES_STATE_POST_GAME then
		return nil
	end
	return 1
end


function CAddonWormWarGameMode:AddSelfEvents()
	ListenToGameEvent("entity_killed", Dynamic_Wrap(CAddonWormWarGameMode,"OnEntityKilled"), self)
	ListenToGameEvent("npc_spawned", Dynamic_Wrap(CAddonWormWarGameMode, "OnNPCSpawned"), self)
end

function CAddonWormWarGameMode:OnEntityKilled(keys)
	local unit = EntIndexToHScript(keys.entindex_killed)
	local tag = unit:GetContext("mytag")
	if tag then
		if tag == "sheep" or tag == "cow" or tag == "fireman" then
			creatunit(tag)
		end
	end
end

function WormWar:OnNPCSpawned( keys )
   local unit =  EntIndexToHScript(keys.entindex)
   
   if unit:IsHero() then
		local playerid=unit:GetPlayerOwnerID()
		PlayerStats[playerid]['group']={}
		PlayerStats[playerid]['group_pointer']=1
		PlayerStats[playerid]['group'][PlayerStats[playerid]['group_pointer']]=unit
		GameRules:GetGameModeEntity():SetContextThink(	DoUniqueString("1"), 
												        function()
															local chaoxiang=unit:GetForwardVector()
															--local truechaoxiang=chaoxiang:Normalized()
															local position=unit:GetAbsOrigin()
															unit:MoveToPosition(position+chaoxiang*500)

															local aroundit=FindUnitsInRadius(	DOTA_TEAM_NEUTRALS, position, nil, 
															  	                              100,
															                          		  DOTA_UNIT_TARGET_TEAM_FRIENDLY,
															                      			  DOTA_UNIT_TARGET_ALL,
															                      			  DOTA_UNIT_TARGET_FLAG_NONE,
															                      			  FIND_ANY_ORDER,
															                      			  false )
															for k,v in pairs(aroundit) do
																local lable=v:GetContext("mytag")
																if lable then
																	if lable=="sheep" then
																		v:ForceKill(true)
																		createbaby(playerid)
																	end
																	if lable=="cow" then
																		v:ForceKill(true)
																		createbaby(playerid)
																		createbaby(playerid)
																	end 	
																end	
															end
															return 0.5
												        end,0)  
   end
end	