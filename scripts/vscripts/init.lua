
function initplayerstats()
    myrandom()

    PlayerStats = {}
    for i = 1, 9 do 
        PlayerStats[i] = {}
        PlayerStats[i]["changdu"]= 0
    end

    --初始化刷怪
    local lefttop = Entity:FindByName("nil","LeftTop")
    local rightdown = Entity:FindByName("nil","RightDown")
    lefttopVec = lefttop:GetAbsOrigin()
    rightdownVec = rightdown:GetAbsOrigin() 

    creatunit("sheep")
    creatunit("sheep")
    creatunit("sheep")
    creatunit("cow")
    creatunit("cow")
    creatunit("fireman")
end

function creatunit(name)
    local xAix = math.random(rightdown.x - lefttop.x) + lefttop.x
    local yAix = math.random(rightdown.y - lefttop.y) + lefttop.y
    local point = Vector(xAix,yAix,lefttop.z)
    local unit = CreateUnitByName(name,point,true,nil,nil,DOTA_TEAM_NEUTRALS)
    unit:SetContext("mytag",name,0)
end

function createbaby(playerid)
    local followed_unit = PlayerStats[playerid]['group'][PlayerStats[playerid]['group_pointer']]
    local dir = followed_unit:GetForwardVector()
    local position = followed_unit:GetAbsOrigin()
    local newposition = position-dir*100


    local new_unit = CreateUnitByName("littlebug", newposition, true, nil, nil, followed_unit:GetTeam())
    new_unit:SetForwardVector(dir)
    GameRules:GetGameModeEntity():SetContextThink(  DoUniqueString("1"),
                                                    function ()
                                                        new_unit:MoveToNPC(followed_unit)
                                                        return 0.2
                                                    end,0) 
    --new_unit:SetControllableByPlayer(playerid, true)
    PlayerStats[playerid]['group_pointer']=PlayerStats[playerid]['group_pointer']+1
    PlayerStats[playerid]['group'][PlayerStats[playerid]['group_pointer']]=new_unit
end